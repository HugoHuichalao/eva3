/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.registropersonas.dao.PersonasJpaController;
import com.mycompany.registropersonas.dao.exceptions.NonexistentEntityException;
import com.mycompany.registropersonas.entity.Personas;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hugo
 */
@WebServlet(name = "ingresoController", urlPatterns = {"/ingresoController"})
public class ingresoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ingresoController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ingresoController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("doPost");
        String accion=request.getParameter("accion");
        if(accion.equals("ingresar")){
                
        try {
            String rut=request.getParameter("rut");
            String nombre=request.getParameter("nombre");
            String edad=request.getParameter("edad");
            String genero=request.getParameter("genero");
            String telefono=request.getParameter("telefono");
            
            Personas    personas =new Personas();
            personas.setRut(rut);
            personas.setNombre(nombre);
            personas.setGenero(genero);
            personas.setTelefono(telefono);
            personas.setEdad(edad);
            
            PersonasJpaController dao=new PersonasJpaController();
            dao.create(personas);
            
            
            
            
            
            
        } catch (Exception ex) {
            Logger.getLogger(ingresoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
       if(accion.equals("verlista")){
            PersonasJpaController dao=new PersonasJpaController();
            List<Personas> lista=dao.findPersonasEntities();
            request.setAttribute("Lista de Ingreso", lista);
            request.getRequestDispatcher("listaIngresos.jsp").forward(request, response);
            
            
            
        }
        if(accion.equals("eliminar")){
            try {
                String rut=request.getParameter("selection");
                PersonasJpaController dao=new PersonasJpaController();
                dao.destroy(rut);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(ingresoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(accion.equals("consultar")){
            String rut=request.getParameter("selection");
            PersonasJpaController dao=new PersonasJpaController();
           Personas registro = dao.findPersonas(rut);
           request.setAttribute("Registro", registro);
           request.getRequestDispatcher("consulta.jsp").forward(request, response);
           
                    
            
        }
        if(accion.equals("editar")){
            try {
                String rut=request.getParameter("rut");
                String nombre=request.getParameter("nombre");
                String edad=request.getParameter("edad");
                String genero=request.getParameter("genero");
                String telefono=request.getParameter("telefono");
                
                Personas    personas =new Personas();
                personas.setRut(rut);
                personas.setNombre(nombre);
                personas.setGenero(genero);
                personas.setTelefono(telefono);
                personas.setEdad(edad);
                
                PersonasJpaController dao=new PersonasJpaController();
                dao.edit(personas);
                List<Personas> lista=dao.findPersonasEntities();
                request.setAttribute("Lista de Ingreso", lista);
                request.getRequestDispatcher("listaIngresos.jsp").forward(request, response);
            } catch (Exception ex) {
                Logger.getLogger(ingresoController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
       processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
