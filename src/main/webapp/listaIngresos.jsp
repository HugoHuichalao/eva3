<%-- 
    Document   : listaIngresos
    Created on : 06-05-2021, 21:18:11
    Author     : Hugo
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.mycompany.registropersonas.entity.Personas"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%List<Personas> ingresos=(List<Personas>) request.getAttribute("Lista de Ingreso");
    Iterator<Personas> itingresos=ingresos.iterator();

%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form name="form" action="ingresoController" method="POST">
        <table border="1">
            <thead>
            <th>nombre</th>
            <th>rut</th>
            <th>genero</th>
            <th>telefono</th>
            <th>edad</th>
        </thead>
        <tbody>

            <%while (itingresos.hasNext()) {
                    Personas cm = itingresos.next();%>
            <tr>

                <td><%= cm.getNombre()%></td>
                <td><%= cm.getRut()%></td>
                <td><%= cm.getGenero()%></td>
                <td><%= cm.getTelefono()%></td>
                <td><%= cm.getEdad()%></td>

                <td><input type="radio" name="selection" value="<%= cm.getRut()%>"> </td>

            </tr>
            

            <%}%>

        </tbody>


    </table>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">Eliminar Ingreso</button>
            <button type="submit" name="accion" value="consultar" class="btn btn-success">Ver Registros</button>
        </form>


</body>
</html>
